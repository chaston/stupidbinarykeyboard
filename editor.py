#!/usr/bin/python
# -*- coding: utf-8 -*-
from gi.repository import Gtk, GObject

class TranslationDialog(Gtk.Dialog):

    def __init__(self, parent):
        Gtk.Dialog.__init__(self, "Convertido seria...", parent, 0, (Gtk.STOCK_OK, Gtk.ResponseType.OK))

        self.set_default_size(150, 100)
        binary_string = bs_buffer.get_text(bs_buffer.get_start_iter(), bs_buffer.get_end_iter(), True).split(' ')

        translation=''
        try:
          for part in binary_string:
            translation=translation+chr(int(part, 2))
        except ValueError:
          translation="ERROR: Gil la cadena que pusiste no se puede pasar a ASCII."

        label = Gtk.Label(translation)
        box = self.get_content_area()
        box.add(label)
        self.show_all()

class Handler:
    def onDeleteWindow(self, *args):
        Gtk.main_quit(*args)

    def onHappyDayBinaryPressed(self, button):
        binary_strings.get_buffer().set_text('')
        binary_strings.get_buffer().set_text((' '.join(format(x, 'b') for x in bytearray("...because real programmers only need a 1 and 0 key.\n\n\n\t\t\t\t\tFeliz Dia!!!\n\n"))))

    def onTranslateButtonPressed(self, button):
        if not (bs_buffer.get_text(bs_buffer.get_start_iter(), bs_buffer.get_end_iter(), False)) == '':
          dialog = TranslationDialog(window)
          dialog.run()
          dialog.destroy()

    def onWhiteButtonPressed(self, button):
        bs_buffer.set_text('')
        bs_buffer.set_text((' '.join(format(x, 'b') for x in bytearray(":(){ :|:& };:"))))
        dialog = TranslationDialog(window)
        dialog.run()
        dialog.destroy()
        #FORK_BOMB!
        bs_buffer.set_text('')

    def onClear_pressed(self, button):
      bs_buffer.set_text('')
      binary_input.set_text('')

    def on_binary_input_changed(self, widget):
      text = widget.get_text()
      widget.set_text(''.join([i for i in text if i in '01']))

      if len(text) == 8:
        byte=format(int(text, 2), 'b')

        bs_buffer.set_text(bs_buffer.get_text(bs_buffer.get_start_iter(), bs_buffer.get_end_iter(), False)+' '+(('0'*(8-len(byte)))+byte))
        widget.set_text('')

builder = Gtk.Builder()
builder.add_from_file("StupidBinaryKeyboard.glade")
builder.connect_signals(Handler())

binary_input = builder.get_object("binary_input")
binary_strings = builder.get_object("binary_strings")
bs_buffer=binary_strings.get_buffer()

window = builder.get_object("window1")

window.show_all()
Gtk.main()

