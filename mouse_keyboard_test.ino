const int zeroButton = 2;
const int oneButton = 3;        
const int spaceButton = 4;
const int enterButton = 5;

void setup() {
  pinMode(zeroButton, INPUT);      
  pinMode(oneButton, INPUT);      
  pinMode(spaceButton, INPUT);      
  pinMode(enterButton, INPUT);
  //Keyboard.begin();
  Serial.begin(9600);
}

void loop() {
  if (digitalRead(zeroButton) == HIGH) {
    //Keyboard.press('0');
    //Keyboard.release('0');
    Serial.println('0');
  }
  if (digitalRead(oneButton) == HIGH) {
    //Keyboard.press('1');
    //Keyboard.release('1');
    Serial.println('1');
  }
  if (digitalRead(spaceButton) == HIGH) {
    //Keyboard.press(' ');
    //Keyboard.release(' ');
    Serial.println(' ');
  }
  if (digitalRead(enterButton) == HIGH) {
    //Keyboard.press(KEY_RETURN);
    //Keyboard.release(KEY_RETURN);
    Serial.println(KEY_RETURN);
  }
  delay(150);
}
